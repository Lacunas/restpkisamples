<!DOCTYPE html>
<html>
<head>
    <?php include 'head.php' ?>
</head>
<body>

    <?php include 'top.php' ?>

	<div class="container">
		
		<h2>Lacuna PKI Samples</h2>
		Choose one of the following samples:
		<ul>
			<li><a href="authentication.php">Authentication with digital certificate</a></li>
			<li><a href="pades-signature.php">Create a PAdES signature</a></li>
		</ul>

	</div>
</body>
</html>
