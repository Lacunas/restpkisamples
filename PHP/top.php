<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">Lacuna PKI Samples</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href="authentication.php">Authentication</a></li>
				<li><a href="pades-signature.php">PAdES Signature</a></li>
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</nav>
<div class="container">
	<div id="messagesPanel"></div>
</div>
